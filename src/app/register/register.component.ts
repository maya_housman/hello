import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  password:string;

  onSubmit(){
    this.auth.register(this.email,this.password);
  }

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
  }

}