// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBPhxvUi85deYhZuVuDUff4usAWTVDHLLU",
    authDomain: "hello-maya.firebaseapp.com",
    databaseURL: "https://hello-maya.firebaseio.com",
    projectId: "hello-maya",
    storageBucket: "hello-maya.appspot.com",
    messagingSenderId: "608764297636",
    appId: "1:608764297636:web:a1a658909c7a47ae7f8131"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
